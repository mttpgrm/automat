package au.edu.mq.comp.dot

import org.bitbucket.inkytonik.kiama.util.{ TestCompilerWithConfig }

import DOTSyntax.DotSpec
import au.edu.mq.comp.automat.auto.NFA
import au.edu.mq.comp.automat.edge.LabDiEdge
import au.edu.mq.comp.automat.util.PathUtil.joinPath

/**
 * Tests that check that the DOT parser and pretty-printers build
 * the correct trees that pretty-print to the correct output.
 */
class ParsingTests extends Driver with TestCompilerWithConfig[ DotSpec, DOTParserConfig ] {

    val path = au.edu.mq.comp.automat.util.PathUtil.dotTestPath
    // filetests( "Parse DOT files", path, ".dot", ".out",
    // argslist = List( Array( "-p" ) ) )
}

import org.scalatest._

import util.DotParser
import org.bitbucket.inkytonik.kiama.util.FileSource

class ParseFileTests extends FunSuite with Matchers {
    test( "Read same DOT file twice and build two AST - Check they are equal" ) {
        val path = au.edu.mq.comp.automat.util.PathUtil.dotTestPath

        //  read a DOT file and create the automat struct
        val parser = new DotParser()

        val parsed1 = parser.makeast( FileSource( joinPath ( path, "auto1.dot" ) ), parser.createConfig( args = List() ) )
        assert( parsed1.isLeft )
        //  read another DOT file and create the automat struct
        val parsed2 = parser.makeast( FileSource( joinPath ( path, "auto1.dot" ) ), parser.createConfig( args = List() ) )
        assert( parsed2.isLeft )

        val a1 = parsed1.left
        val a2 = parsed2.left

        assert( a1 == a2 )
    }
}

import au.edu.mq.comp.dot.util.DotParser

import scala.util.{ Try, Success, Failure }
import util.DotASTAttr._
import util.FileParser.parseFile

/**
 * Parse DOT file and create NFA
 *
 */
class DotToNFATests extends FunSuite with Matchers {

    val path = au.edu.mq.comp.automat.util.PathUtil.dotTestPath

    test( "Parse auto1 and build an NFA - Check initial, final" ) {
        //  parse auto1.dot
        val tryNfa : Try[ DotSpec ] = parseFile( joinPath( path, "auto1.dot" ) )
        assert( tryNfa.isSuccess )
        //  build an NFA
        val nfa1 = NFA( getInitStates( tryNfa.get ), getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        import nfa1._
        //  check initial
        assert( getInit === Set( "node1" ) )
        //  check final
        assert( !isFinal( succ( getInit, "" ) ) )
        assert( isFinal( succ( succ( getInit, "" ), "a" ) ) )
    }

    test( "Parse auto2 and build an NFA - Check initial, final" ) {
        //  parse auto1.dot
        val tryNfa : Try[ DotSpec ] = parseFile( joinPath ( path, "auto2.dot" ) )
        assert( tryNfa.isSuccess )
        //  build an NFA
        val nfa2 = NFA( getInitStates( tryNfa.get ), getEdges( tryNfa.get ), getAcceptStates( tryNfa.get ) )
        import nfa2._
        //  check initial
        assert( getInit === Set( "node_0" ) )
        //  check final
        assert( !isFinal( succ( getInit, "1_M" ) ) )
        assert( isFinal( succ( succ( getInit, "1_M" ), "1_M" ) ) )
    }
}
