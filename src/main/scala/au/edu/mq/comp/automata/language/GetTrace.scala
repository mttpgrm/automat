/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package lang

import util.DFSVisitor

/**
 * Visitor to collect an accepting trace in an automaton (if any.
 *
 * @param   S           Type of the states of the automatan.
 * @param   L           Type of the alphabet
 * @param   xl          Sequence of labels seen on the stack during
 *                      the DFS in rerverse order.
 * @param   accepted    `true` if the sequence xl.reversed leads to a final state.
 * @param   isFinal     The predicate that decides whether a given state is final or bot.
 */
private case class RecordTraceVisitor[ S, L ](
        val xl : Seq[ L ],
        val accepted : Boolean,
        isFinal : S ⇒ Boolean
) extends DFSVisitor[ S, L, RecordTraceVisitor[ S, L ] ] {

    //  new state `s` is discovered. Update visitor status with `isFinal(s)`
    override def discoverState( s : S, d : Map[ S, Int ], p : Map[ S, ( S, L ) ] ) = {
        RecordTraceVisitor( xl, isFinal( s ), isFinal )
    }

    //  State is finished and cannot be final as we would have stopped the
    //  search when it was first discovered. We can pop the most recent
    //  label (if any) as DFS will pop a state and continue the search.
    override def finishState( s : S, d : Map[ S, Int ], p : Map[ S, ( S, L ) ] ) = {
        //  pop most recent letter
        val h = if ( xl.isEmpty ) xl else xl.tail
        //  compute the letter for debugging purposes
        RecordTraceVisitor( h, accepted, isFinal )
    }

    //  push label l onto the stack i.e. prepend to xl
    override def treeEdge( s : S, l : L, t : S, d : Map[ S, Int ], p : Map[ S, ( S, L ) ] ) = {
        RecordTraceVisitor( l +: xl, accepted, isFinal )
    }

    //  condition to stop the DFS
    override def abortSearch = accepted

}
