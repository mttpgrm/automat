/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package lang

import auto.DetAuto

/**
 * Provide support for languages accepted by [[auto.DetAuto]]
 */
case class Lang[ S, L ]( private val a : DetAuto[ S, L ], private val prefix : Seq[ L ] = List() ) {

    /**
     * Union of languages.
     */
    def \/[ S1 ]( l2 : Lang[ S1, L ] ) = Lang( this.a + l2.a )

    /**
     * Intersection of languages.
     */
    def /\[ S1 ]( l2 : Lang[ S1, L ] ) = Lang( this.a * l2.a )

    /**
     * Difference of languages.
     */
    def \[ S1 ]( l2 : Lang[ S1, L ] ) = Lang( this.a - l2.a )

    /**
     * Prefix quotient.
     */
    def /[ S1 ]( xl : Seq[ L ] ) = Lang( this.a, xl )

    /**
     * Language emptiness.
     */
    lazy val isEmpty : Boolean = getAcceptedTrace.isEmpty

    /**
     * Language equality.
     */
    def ===[ S1 ]( l2 : Lang[ S1, L ] ) : Boolean = ( l2 \ this ).isEmpty && ( this \ l2 ).isEmpty

    /**
     * Language inclusion.
     */
    def <=[ S1 ]( l2 : Lang[ S1, L ] ) : Boolean = ( this \ l2 ).isEmpty

    import a._
    /**
     * Membership check.
     *
     * @param   xl  A prefix trace
     * @return      `true` if the trace is in the language accepted by `a`.
     */
    def accepts( xl : Seq[ L ] ) : Boolean = a.isFinal( a.succ( a.getInit, prefix ++ xl ) )

    /**
     * Compute an accepted trace if any
     */
    lazy val getAcceptedTrace : Option[ Seq[ L ] ] = getAcceptedTraceAfter( List() )

    /**
     * Look for a suffix `s` of a given trace `xl` such that `s.xl`
     * is accepted by `a`.˙
     */
    def getAcceptedTraceAfter( xl : Seq[ L ] ) : Option[ Seq[ L ] ] = {
        import util.Traversal.DFS

        //  perform a DFS with the RecordTrace visitor from the initial state
        val v = RecordTraceVisitor[ S, L ]( List(), false, isFinal )
        val dfsForAcc = DFS( v, a.outGoingEdges )
        val r = dfsForAcc( succ( getInit, xl ) )

        if ( r.accepted )
            Some( r.xl.reverse )
        else
            None
    }
}
