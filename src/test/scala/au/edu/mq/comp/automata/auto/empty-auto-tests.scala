package au.edu.mq.comp.automat

package auto

import org.scalatest._
import au.edu.mq.comp.automat.edge.Implicits._
import au.edu.mq.comp.automat.edge.LabDiEdge
import util.Traversal.DFS
import util.DotConverter._
import util.DefaultDFSVisitor
import util.Determiniser.toDetNFA
import au.edu.mq.comp.dot.DOTPrettyPrinter.format

class EmptyAutoTests extends FunSuite with Matchers {

    override def suiteName = "Check DetAuto methods on NFA with empty language"

    test( "All empty, check final, accepting, enabled in, succ" ) {
        //  has one initial state but no transition
        val a1 = NFA[ Int, String ]( Set(), Set(), Set() )
        assert( a1.getInit === Set() )
        assert( a1.acceptsNone( a1.getInit ) )
        assert( a1.enabledIn( a1.getInit ) === Set() )
        assert( !a1.isFinal( a1.getInit ) )
        assert( !a1.acceptsAll( a1.getInit ) )
        assert( a1.succ( a1.getInit, "a" ) === a1.getInit )
        assert( a1.succ( a1.getInit, Seq( "a", "b", "a" ) ) === a1.getInit )
        assert( a1.outGoingEdges( a1.getInit ) === List() )
    }

    test( "Empty set of edges, one state, check final, accepting, enabled in, succ" ) {
        //  has one initial state but no transition
        val a1 = NFA[ Int, String ]( Set( 0 ), Set(), Set() )

        assert( a1.getInit === Set( 0 ) )
        assert( a1.acceptsNone( a1.succ( a1.getInit, "b" ) ) )
        assert( a1.enabledIn( a1.getInit ) === Set() )
        assert( !a1.isFinal( a1.getInit ) )
        assert( !a1.acceptsAll( a1.getInit ) )
        assert( a1.succ( a1.getInit, "a" ) === a1.succ( a1.getInit, Seq( "a", "b" ) ) )
        assert( a1.outGoingEdges( a1.getInit ) === List() )

    }

    test( "One loop, deterministic" ) {
        //  deterministic automaton with a loop
        val a2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "b" ), ( 2 ~> 1 )( "a" ), ( 2 ~> 2 )( "c" ) ),
            Set()
        )
        assert( a2.getInit === Set( 0 ) )
        assert( a2.acceptsNone( a2.succ( a2.getInit, "b" ) ) )
        assert( a2.enabledIn( a2.getInit ) === Set( "a" ) )
        assert( !a2.isFinal( a2.getInit ) )
        assert( !a2.acceptsAll( a2.getInit ) )
        assert( a2.succ( a2.getInit, "a" ) === a2.succ( a2.getInit, Seq( "a", "b", "a" ) ) )
        assert( a2.outGoingEdges( a2.succ( a2.getInit, Seq( "a", "b" ) ) ) === List( ( "a", Set( 1 ) ), ( "c", Set( 2 ) ) ) )

    }

    test( "One loop, non deterministic" ) {
        // non detrrministic automaton
        val a3 = NFA(
            Set( 0, 1 ),
            Set( ( 0 ~> 2 )( "a" ), ( 2 ~> 3 )( "a" ), ( 1 ~> 4 )( "a" ), ( 4 ~> 4 )( "b" ) ),
            Set( 3, 4 )
        )

        assert( toDetNFA( a3 ) ===
            NFA(
                Set( 0 ),
                Set( ( 2 ~> 2 )( "b" ), ( 1 ~> 2 )( "b" ), ( 1 ~> 3 )( "a" ), ( 0 ~> 1 )( "a" ) ),
                Set( 1, 2, 3 )
            ) )
    }

    // test( "Intersection,  " ) {
    //     // non deterministic automaton
    //     val a3 = NFA(
    //         Set( 0, 1 ),
    //         Set( ( 0 ~> 2 )( "c" ), ( 2 ~> 3 )( "a" ), ( 1 ~> 4 )( "a" ), ( 4 ~> 4 )( "b" ) ),
    //         Set( 3 )
    //     )

    //     val a4 = NFA[ Int, String ](
    //         Set( 0 ),
    //         Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "b" ), ( 2 ~> 1 )( "a" ), ( 2 ~> 2 )( "c" ) ),
    //         Set( 2 )
    //     )

    //     println( toDetNFA( a3 ) )
    //     println( format( toDot( toDetNFA( a3 * a4 ) ) ).layout )

    //     assert( toDetNFA( a3 * a4 ) ==
    //         NFA(
    //             Set( 0 ),
    //             Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "b" ) ),
    //             Set()
    //         ) )
    // }

}
