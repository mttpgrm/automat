package au.edu.mq.comp.automat

package util

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import Traversal.DFS
import Determiniser.toDetNFA
import au.edu.mq.comp.dot.DOTPrettyPrinter.format
import DotConverter._
import au.edu.mq.comp.dot.DOTSyntax.DotSpec
import PathUtil.tmpPath
import reflect.io._

class DeterminiserTests extends FunSuite with Matchers {

    test( "Determinise an NFA with no transition which is a DFA, check equality" ) {
        //  first one has no transition
        val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set() )
        val det = toDetNFA( nfa1 )
        assert( nfa1 == det )
    }

    test( "Determinise an NFA and check result" ) {
        //  second one
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 2 )( "a" )
            ),
            Set( 2 )
        )
        val det = toDetNFA( nfa2 )

        val result = NFA(
            Set( 0 ),
            Set(
                ( 1 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 0 ~> 1 )( "a" )
            ),
            Set( 1, 2 )
        )

        assert( result == det )
    }

    test( "Automata with acceptsNone states" ) {
        //  second one
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "b" ), ( 2 ~> 3 )( "c" ), ( 2 ~> 1 )( "a" )
            ),
            Set( 2 ),
            Set(),
            Set( 3 )
        )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "c" ) ),
            Set( 2 ),
            Set( 2 )
        )
        val r = toDetNFA( nfa1 + nfa2 )
        val rDot : DotSpec = toDot( r )
        //  print automaton in a file
        File( tmpPath( "r.dot" ) ).writeAll( format( rDot ).layout )
    }
}
