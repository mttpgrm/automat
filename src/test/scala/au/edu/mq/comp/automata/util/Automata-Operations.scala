package au.edu.mq.comp.automat

package util

import org.scalatest._

import auto.NFA
import edge.LabDiEdge
import edge.Implicits._
import Traversal.DFS

import util.Determiniser.toDetNFA

import scala.util.{ Try, Success, Failure }
import au.edu.mq.comp.dot.util.DotASTAttr._

import au.edu.mq.comp.dot.util.FileParser.parseFile
import au.edu.mq.comp.dot.DOTSyntax.DotSpec

import au.edu.mq.comp.dot.DOTPrettyPrinter.format

import reflect.io._

class AutomataIntersectionTests extends FunSuite with Matchers {
    val path = "src/test/scala/au/edu/mq/comp/automata/dot-test-files/"

    test( "Intersetion/Union of two NFAs with no common first letter" ) {
        //  build an NFA. As we read strings we have to convert
        val nfa1 = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "auto4.dot" )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get )
            )
        }

        //  build an NFA. As we read strings we have to convert
        val nfa2 = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "auto5.dot" )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get )
            )
        }

        val d = nfa1 * nfa2

        assert( toDetNFA( d ) == NFA[ Int, String ]( Set( 0 ), Set(), Set() ) )

        val e = nfa1 + nfa2
        val dete : NFA[ Int, String ] = toDetNFA( e )

        val resForE : NFA[ Int, String ] = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "res1.dot" )
            assert( tryNfa.isSuccess )
            //  map the String obtained by parsing to Int
            NFA(
                getInitStates( tryNfa.get ).map( _.toInt ),
                getEdges( tryNfa.get ) map {
                    case LabDiEdge( e, l ) ⇒ ( e.src.toInt ~> e.tgt.toInt )( l )
                }, getAcceptStates( tryNfa.get ).map( _.toInt )
            )
        }

        assert( toDetNFA( e ) == resForE )
    }

    test( "Intersetion/Union of two NFAs" ) {
        //  build an NFA. As we read strings we have to convert
        val nfa1 = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "auto4.dot" )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get )
            )
        }

        //  build an NFA. As we read strings we have to convert
        val nfa2 = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "auto5.dot" )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get )
            )
        }

        val d = nfa1 * nfa2

        assert( toDetNFA( d ) == NFA[ Int, String ]( Set( 0 ), Set(), Set() ) )

        val e = nfa1 + nfa2

        val resForE : NFA[ Int, String ] = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "res2.dot" )
            assert( tryNfa.isSuccess )
            //  map the String obtained by parsing to Int
            NFA(
                getInitStates( tryNfa.get ).map( _.toInt ),
                getEdges( tryNfa.get ) map {
                    case LabDiEdge( e, l ) ⇒ ( e.src.toInt ~> e.tgt.toInt )( l )
                }, getAcceptStates( tryNfa.get ).map( _.toInt )
            )
        }

        assert( toDetNFA( e ) == resForE )
    }

    test( "Two NFAs and compute difference" ) {
        //  build an NFA. As we read strings we have to convert
        val nfa1 = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "auto6.dot" )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get )
            )
        }

        //  build an NFA. As we read strings we have to convert
        val nfa2 = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "auto7.dot" )
            assert( tryNfa.isSuccess )
            NFA(
                getInitStates( tryNfa.get ),
                getEdges( tryNfa.get ), getAcceptStates( tryNfa.get )
            )
        }

        val d = nfa1 - nfa2
        val resFord : NFA[ Int, String ] = {
            val tryNfa : Try[ DotSpec ] = parseFile( path + "res3.dot" )
            assert( tryNfa.isSuccess )
            //  map the String obtained by parsing to Int
            NFA(
                getInitStates( tryNfa.get ).map( _.toInt ),
                getEdges( tryNfa.get ) map {
                    case LabDiEdge( e, l ) ⇒ ( e.src.toInt ~> e.tgt.toInt )( l )
                }, getAcceptStates( tryNfa.get ).map( _.toInt )
            )
        }

        assert( toDetNFA( d ) == resFord )
    }

}
