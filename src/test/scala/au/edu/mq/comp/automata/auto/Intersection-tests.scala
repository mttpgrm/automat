package au.edu.mq.comp.automat

package auto

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._

class IntersectionTests extends FunSuite with Matchers {
    //  first one has no transition
    val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set() )
    //  second one
    val nfa2 = NFA[ Int, String ](
        Set( 0 ),
        Set(
            ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 2 )( "a" )
        ),
        Set( 2 )
    )

    val p1 = nfa1 * nfa2
    val initState = p1.getInit

    // test( "Check empty states" ) {
    //     import p1.{ succ }

    //     //  succ after a.a should not be dummy
    //     val s3 = succ( succ( initState, "a" ), "a" )
    //     assert( s3.nonEmpty )
    //     val s4 = succ( s3, "d" )
    //     assert( s4.isEmpty )
    // }

    test( "Check final states" ) {
        import p1.{ succ, isFinal }

        //  succ after a.a should not be final state (no final in SyncProd)
        val s3 = succ( succ( initState, "a" ), "a" )
        //
        assert( !isFinal( s3 ) )
        assert( !isFinal( initState ) )
        assert( !isFinal( succ( initState, "b" ) ) )
    }

    test( "Check succ for a sequence vs multiple succ for a single letter" ) {
        import p1.{ succ }

        //  succ after a.a
        val s3 = succ( succ( initState, "a" ), "a" )

        // succ using sequences
        val s3copy = succ( initState, List( "a", "a" ) )
        //  should be the same state
        assert( s3copy == s3 )

        //  empty sequence of letters
        val s4 = succ( s3, List() )
        assert( s4 == s3 )
    }

}
