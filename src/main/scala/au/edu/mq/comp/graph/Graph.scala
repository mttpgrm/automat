/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package graph

import edge.{ DiEdge }

/**
 * A Directed Graph (DiGraph) is defined by a set of (unlabelled) edges.
 */
class DiGraph[ S ]( private[ automat ] val edges : Set[ DiEdge[ S ] ] ) {

    //  set of directed edges in the form s - None -> {s1,s2, ..., sn}
    lazy private val nextMap : Map[ S, Set[ S ] ] = {
        edges.groupBy( e ⇒ e.src ) map {
            case ( k, xv ) ⇒ ( k, xv.map( _.tgt ).toSet )
        }
    }

    //  set of directed edges in the form s - None -> {s1,s2, ..., sn}
    lazy private val predMap : Map[ S, Set[ S ] ] = {
        edges.groupBy( e ⇒ e.tgt ) map {
            case ( k, xv ) ⇒ ( k, xv.map( _.src ).toSet )
        }
    }

    /**
     * Compute the successor nodes
     *
     * @param  s   A node
     * @return     Set of nodes `n` such there exists an edge from
     * `s` to `n`
     */
    def next( s : S ) : Set[ S ] = nextMap.getOrElse( s, Set() )

    /**
     * Compute the predecessor nodes
     *
     * @param  s   A node
     * @return     Set of nodes `n` such there exists an edge from
     * `n` to `s`
     */
    def pred( s : S ) : Set[ S ] = predMap.getOrElse( s, Set() )

    /**
     * Reverse a graph
     *
     * The new graph is the same as the subject graph with edges reversed.
     */
    def reversed : DiGraph[ S ] = new DiGraph[ S ]( edges map ( _.reversed ) )
}

/**
 * A rooted graph
 *
 * @param  root   A node of `g` that is designated as the root of `g`
 * @param  g      A graph.
 */
case class RootedDiGraph[ S ]( root : S, g : DiGraph[ S ] ) extends DiGraph[ S ]( g.edges ) {

    override def reversed : RootedDiGraph[ S ] = new RootedDiGraph( root, g.reversed )
}

