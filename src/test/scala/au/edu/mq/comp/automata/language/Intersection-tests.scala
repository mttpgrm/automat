package au.edu.mq.comp.automat

package lang

import util.Traversal.DFS
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import scala.language.existentials

import org.scalatest._

class InterLangTests extends FunSuite with Matchers {

    test( "Intersection of two languages, one empty" ) {
        //  one automaton, empty
        val nfa1 = NFA[ Int, String ]( Set(), Set(), Set() )
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 1 ~> 2 )( "a" ), ( 0 ~> 1 )( "b" ) ),
            Set( 2 )
        )

        val l = Lang( nfa1 ) /\ Lang( nfa2 )

        assert( l.isEmpty )

    }

    test( "Intersection of two languages, both non empty" ) {
        //  one automaton, empty
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 1 )( "a" ) ),
            Set( 1 )
        )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 1 ~> 2 )( "b" ), ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "a" ) ),
            Set( 2 )
        )

        val l = Lang( nfa1 ) /\ Lang( nfa2 )

        assert( !l.isEmpty )

        val e = l.getAcceptedTrace

        assert( l accepts List( "a", "a" ) )
        assert( !( l accepts List( "a", "b" ) ) )
    }

    test( "Intersection of two automata - empty string not accepted" ) {
        //  one automaton, accepts empty string
        val nfa1 = NFA[ Int, String ]( Set( 0 ), Set(), Set( 0 ) )
        //  another one does not accept empty string
        val nfa2 = NFA[ Int, String ]( Set( 0 ), Set(), Set() )

        val l = Lang( nfa1 ) /\ Lang( nfa2 )

        assert( l.isEmpty )

        assert( Lang( nfa1 ) accepts List() )
        assert( !( Lang( nfa2 ) accepts List() ) )
        assert( !( l accepts List() ) )
    }

}
