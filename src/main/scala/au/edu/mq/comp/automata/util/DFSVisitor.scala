/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package util

/**
 * Provide default methods for visitors to DFS
 *
 * @tparam  S   Type of states
 * @tparam  T   Type of labels
 * @tparam  V   Type of the visitor itself
 *
 * @note This trait definition uses  F-bounded polymorphism to return a visitor
 * of type the subtype of this trait.
 */
trait DFSVisitor[ S, L, V <: DFSVisitor[ S, L, V ] ] {

    /**
     * Applied after a state has been fully explored (all
     * its successors have been DFsearched).
     */
    def finishState( s : S, discoveryTime : Map[ S, Int ], parent : Map[ S, ( S, L ) ] ) : V = this.asInstanceOf[ V ]

    /**
     *   Applied when a state is first discovered (it is `White`) in the DFS
     *   and <strong>before</strong> it is expanded and actually explored.
     */
    def discoverState( s : S, discoveryTime : Map[ S, Int ],
        parent : Map[ S, ( S, L ) ] ) : V = this.asInstanceOf[ V ]

    /**
     * Applied when an edge (src, l, tgt) is processed and
     * target tgt is `White` (not already explored).
     */
    def treeEdge( s : S, l : L, t : S, discoveryTime : Map[ S, Int ], parent : Map[ S, ( S, L ) ] ) : V = this.asInstanceOf[ V ]

    /**
     * Applied when an edge (s,l, t) is processed and
     * t is `Gray` (on the stack).
     */
    def backEdge( s : S, l : L, t : S, discoveryTime : Map[ S, Int ], parent : Map[ S, ( S, L ) ] ) : V = this.asInstanceOf[ V ]

    /**
     * Applied when an edge (s, l, t) is
     * processed and tgt is `Black` (has been already fully explored
     * and is not on the stack anymore).
     * (it is sometimes called `crossEdge`)
     */
    def forwardEdge( s : S, l : L, t : S, discoveryTime : Map[ S, Int ], parent : Map[ S, ( S, L ) ] ) : V = this.asInstanceOf[ V ]

    /**
     * This flag is checked by the DFS algorithm everytime a new
     * state is about processed. DFS terminates when the visitor
     * has this flag true. DFS then returns a visitor object with the same
     * value as the one that was passed in.
     */
    def abortSearch = false
}

/**
 * Collect the default value of the DFS, discovery time of a node, and parent map
 *
 * @param discoveryTime Map that gives the logical discovery time of a node
 * @param parent Map that gives the parent of node (except root) in the DFS spanning tree
 */
class DefaultDFSVisitor[ S, L ](
        val discoveryTime : Map[ S, Int ] = Map[ S, Int ](),
        val parent : Map[ S, ( S, L ) ] = Map[ S, ( S, L ) ]()
) extends DFSVisitor[ S, L, DefaultDFSVisitor[ S, L ] ] {

    override def discoverState( s : S, discoveryTimeMap : Map[ S, Int ],
        parentMap : Map[ S, ( S, L ) ] ) = {
        DefaultDFSVisitor( discoveryTimeMap, parentMap )
    }

    override def finishState( s : S, discoveryTimeMap : Map[ S, Int ],
        parentMap : Map[ S, ( S, L ) ] ) = {
        DefaultDFSVisitor( discoveryTimeMap, parentMap )
    }

    override def treeEdge( s : S, l : L, t : S,
        discoveryTimeMap : Map[ S, Int ], parentMap : Map[ S, ( S, L ) ] ) = {
        DefaultDFSVisitor( discoveryTimeMap, parentMap )
    }

    override def backEdge( s : S, l : L, t : S,
        discoveryTimeMap : Map[ S, Int ], parentMap : Map[ S, ( S, L ) ] ) = {
        DefaultDFSVisitor( discoveryTimeMap, parentMap )
    }

    override def forwardEdge( s : S, l : L, t : S,
        discoveryTimeMap : Map[ S, Int ], parentMap : Map[ S, ( S, L ) ] ) = {
        DefaultDFSVisitor( discoveryTimeMap, parentMap )
    }
}

/**
 * Default DFSVisitor.
 *
 */
object DefaultDFSVisitor {

    import auto.DetAuto

    /**
     * Create an empty default DFS visitor of type compatible with a [[auto.DetAuto]]
     *
     * @param a A [[auto.DetAuto]]
     * @return Return A DFSVisitor of type compatible with `a`
     */
    def apply[ S, L ]( a : DetAuto[ S, L ] ) = new DefaultDFSVisitor[ S, L ]()

    /**
     * Proxy to create DefaultDFSVisitor
     *
     * @param d A discovery time map
     * @param p A parent map
     * @return A DefaultDFSVisitor initialised with `(d, p)`
     */
    def apply[ S, L ]( d : Map[ S, Int ], p : Map[ S, ( S, L ) ] ) = new DefaultDFSVisitor( d, p )
}
