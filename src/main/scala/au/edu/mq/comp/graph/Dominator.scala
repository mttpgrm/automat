/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package graph

/**
 * Provide functions to compute dominators, immediate dominators,
 * and dominance frontiers.
 */
object Dominator {

    import util.Traversal._
    import util.DefaultDFSVisitor
    import edge.{ LabDiEdge }

    /**
     * Compute the immediate dominator tree for a  [[RootedDiGraph]] `g`.
     *
     * @return  the  map that defines the immediate dominator for `g`,
     *          `iDom: S -> S` with iDom(s) the immediate dominator of `s`.
     *          The root `g.root` does not have an immediate dominator.
     */
    def tarjanImDom[ S ]( g : RootedDiGraph[ S ] ) : Map[ S, S ] = {
        //  compute semi and vertex (as defined in Tarjan and al. paper )
        //  performs a DFS with the default visitor and collect parent, discoveryTime
        //  step 1, DFS
        //  create a visitor for the graph-NFA
        //  the label of the edges doea not matter (not used)
        //  and we use None in the DFS.
        val r = DFS(
            new DefaultDFSVisitor[ S, Option[ Nothing ] ](),
            { x : S ⇒ g.next( x ).toList.map( ( None : Option[ Nothing ], _ ) ) }
        )( g.root )

        //  check Dominator: pred and next are swapped somewhere and this was lost
        //  when using RootedDiGraph

        //  now r.discovery time and r.parent contains
        //  the vertex number and the parent map
        //  maps used in Tarjan's algorithm
        val vertex : Map[ Int, S ] = r.discoveryTime map { case ( k, v ) ⇒ ( v, k ) }
        val number : Map[ S, Int ] = r.discoveryTime map { case ( k, v ) ⇒ ( k, v ) }
        val parent : Map[ S, S ] = r.parent map { case ( s, ( t, l ) ) ⇒ ( s, t ) }

        //  mutable maps. Updated when executing eval, compress etc
        val semi : scala.collection.mutable.Map[ S, Int ] = collection.mutable.Map() ++ number

        val ancestor : scala.collection.mutable.Map[ S, S ] = collection.mutable.Map()

        val label : scala.collection.mutable.Map[ S, S ] =
            collection.mutable.Map() ++
                ( r.discoveryTime map { case ( k, v ) ⇒ ( k, k ) } )

        val bucket : scala.collection.mutable.Map[ S, Set[ S ] ] =
            collection.mutable.Map() ++
                ( r.discoveryTime map { case ( k, v ) ⇒ ( k, Set[ S ]() ) } )

        val dom : scala.collection.mutable.Map[ S, S ] = collection.mutable.Map()

        for ( i ← ( r.discoveryTime.size - 1 to 1 by -1 ) ) {
            val w = vertex( i )
            //  step 2. If a node is unreachable from sink, we ignore it
            for ( v ← g.pred( w ) if number.isDefinedAt( v ) ) {
                val u = eval( v )
                if ( semi( u ) < semi( w ) )
                    semi( w ) = semi( u )
            }

            //   add w to bucket(vertex(semi(w)))
            bucket += ( vertex( semi( w ) ) → ( bucket( vertex( semi( w ) ) ) + w ) )
            //  LINK(parent(w), w). LINK(v,w) equivalent to ancestor(w) := v
            ancestor += ( w → parent( w ) )

            //  step 3
            for ( v ← bucket( parent( w ) ) ) {
                //  delete v from parent(bucket(w))
                bucket += ( parent( w ) → ( bucket( parent( w ) ) - v ) )
                val u = eval( v )
                if ( semi( u ) < semi( v ) )
                    dom += ( v → u )
                else
                    dom += ( v → parent( w ) )
            }
        }

        //  step 4
        for ( i ← ( 1 to r.discoveryTime.size - 1 ) ) {
            val w = vertex( i )
            if ( dom( w ) != vertex( semi( w ) ) )
                dom( w ) = dom( dom( w ) )
        }

        def eval( v : S ) : S = {
            if ( !ancestor.isDefinedAt( v ) )
                v
            else {
                //  compress v
                compress( v )
                label( v )
            }
        }

        //  returns two new maps, label and ancestor
        def compress( v : S ) : Unit = {
            require( ancestor.isDefinedAt( v ) )
            if ( ancestor.isDefinedAt( ancestor( v ) ) ) {
                //  compress ancestor
                compress( ancestor( v ) )
                if ( semi( label( ancestor( v ) ) ) < semi( label( v ) ) )
                    label += ( v → label( ancestor( v ) ) )
                ancestor += ( v → ancestor( ancestor( v ) ) )
            }
        }
        dom.toMap
    }

    /**
     * Compute the dominance frontier for a [[RootedDiGraph]] `g`.
     *
     * @return  The dominance frontier map, `domF(s): S -> Set[S]`.
     *          Node `x` is in `domF(s)` iff 1) `x` has at least two
     *          outgoing edges and 2) one the edge leads to `s` and
     *          another edge may not lead to `s`.
     */

    def domFrontier[ S ]( g : RootedDiGraph[ S ] ) : Map[ S, Set[ S ] ] = {
        // for all nodes, b
        //  if the number of predecessors of b ≥ 2
        //   for all predecessors, p, of b
        //    runner ← p
        //    while runner ̸= doms[b]
        //      add b to runner’s dominance frontier set
        //      runner = doms[runner]

        val iDom = tarjanImDom( g )
        val domFrontier : scala.collection.mutable.Map[ S, Set[ S ] ] =
            collection.mutable.Map() ++
                ( ( iDom.keySet + g.root ).map ( ( _, Set[ S ]() ) ) )

        //    post dominance frontier, Cooper et al. Figure 5
        //  collect nodes in iDom
        val nodes = iDom.keySet
        for ( b ← nodes if g.pred( b ).size >= 2 ) {
            for ( p ← g.pred( b ) if nodes.contains( p ) ) {
                var runner = p
                while ( runner != iDom( b ) ) {
                    domFrontier += ( runner → ( domFrontier.getOrElse( runner, Set() ) + b ) )
                    runner = iDom( runner )
                }
            }
        }
        domFrontier.toMap
    }
}
