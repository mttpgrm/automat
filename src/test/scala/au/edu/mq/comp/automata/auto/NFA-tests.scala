package au.edu.mq.comp.automat

package auto

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._

class NFATests extends FunSuite with Matchers {
    test( "Create an empty NFA" ) {
        val nfa1 = NFA[ Int, String ]( Set(), Set(), Set() )
        assert( nfa1.isFinal( Set( 2 ) ) === false )
        assert( nfa1.succ( nfa1.getInit, "a" ) === Set() )

        val nfa2 = NFA[ Int, String ]( Set( 1, 2 ), Set(), Set() )
        assert( nfa1.isFinal( Set( 2 ) ) === false )
        assert( nfa1.succ( nfa1.getInit, "a" ) === Set() )
    }

    test( "Create an NFA with one init state, no final state" ) {
        val nfa1 = NFA[ Int, Int ]( Set( 1 ), Set( ( 1 ~> 1 )( 0 ) ), Set() )
        val s = nfa1.succ( nfa1.getInit, 0 )
        assert( s != None )
        assert( s === nfa1.getInit )
    }

    test( "Create a cyclic NFA" ) {
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 0 )( "b" ), ( 1 ~> 2 )( "a" )
            ),
            Set( 2 )
        )
        //  get initial state
        val i = nfa1.getInit
        val s = nfa1.succ( i, "b" )
        assert( s === Set() )
        val s1 = nfa1.succ( s, "d" )
        assert( s1 === Set() )
        assert( !nfa1.isFinal( s1 ) )
        val s2 = nfa1.succ( nfa1.succ( i, "a" ), "a" )
        assert( nfa1.isFinal( s2 ) )
    }

}
