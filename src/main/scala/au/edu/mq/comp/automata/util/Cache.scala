/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package util

import org.bitbucket.inkytonik.kiama.util.Memoiser

/**
 * A cahce factory
 */
object Cache extends Memoiser {

    /**
     * Cache of size 's'
     */
    class Cache[ A, B ]( s : Int ) extends Memoised[ A, B ] {
        override val size : Long = s
    }

    def apply[ A, B ]( s : Int = 10000 ) = new Cache[ A, B ]( s )
}
