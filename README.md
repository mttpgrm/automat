
[![Run Status](https://api.shippable.com/projects/57497cfa2a8192902e21b58a/badge?branch=master)](https://app.shippable.com/projects/57497cfa2a8192902e21b58a)
[![Coverage Badge](https://api.shippable.com/projects/57497cfa2a8192902e21b58a/coverageBadge?branch=master)](https://app.shippable.com/projects/57497cfa2a8192902e21b58a)

# Automata library #

This library provides classes and methods to create finite automata and compositions thereof.
To use it, simply clone the project, run sbt test and set publishLocal.

### Automat ###

* The main package is automat. It provides classes and methods to create/compose finite automata.
* Version 0.1


### Set up ###

* Clone the repository
* run sbt test in the root of cloned repository
* you need a recent version of sbt-rats! https://bitbucket.org/inkytonik/sbt-rats and Kiama 2.0 https://bitbucket.org/inkytonik/kiama

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Contact ###

* franck.cassez@mq.edu.au
* Macquarie University, Sydney, Australia
