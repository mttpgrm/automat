package au.edu.mq.comp.automat

package lang

import util.Traversal.DFS
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import scala.language.existentials

import org.scalatest._

class DiffLangTests extends FunSuite with Matchers {

    test( "Diffrence of two languages, one empty" ) {
        //  one automaton, empty
        val nfa1 = NFA[ Int, String ]( Set(), Set(), Set() )
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 1 ~> 2 )( "a" ), ( 0 ~> 1 )( "b" ) ),
            Set( 2 )
        )

        val l = Lang( nfa2 ) \ Lang( nfa1 )

        assert( !l.isEmpty )

        val e = l.getAcceptedTrace
        assert( e.get == List( "b", "a" ) )

    }

    test( "Difference of two languages, both non empty" ) {
        //  one automaton, empty
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 0 )( "b" ) ),
            Set( 1 )
        )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 1 ~> 2 )( "a" ), ( 0 ~> 1 )( "b" ) ),
            Set( 2 )
        )

        val l = Lang( nfa1 ) \ Lang( nfa2 )

        assert( !l.isEmpty )

        val e = l.getAcceptedTrace

        assert( l accepts List( "a", "b", "a" ) )
        assert( !( l accepts List( "a", "b", "a", "a" ) ) )
    }

    test( "Difference of two languages, one prefix of another" ) {
        //  one automaton, empty
        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "b" ) ),
            Set( 2 )
        )

        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set( ( 1 ~> 0 )( "b" ), ( 0 ~> 1 )( "a" ) ),
            Set( 1 )
        )

        val l = Lang( nfa2 ) \ Lang( nfa1 )

        assert( !l.isEmpty )

        val e = l.getAcceptedTrace

        assert( l accepts List( "a", "b", "a" ) )
        assert( !( l accepts List( "a", "b", "a", "a" ) ) )
    }

}
