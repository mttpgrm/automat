/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package util

/**
 * Provide converter from NFA to [[au.edu.mq.comp.dot.DOTSyntax.DotSpec]]
 */
object DotConverter {

    import au.edu.mq.comp.dot.DOTSyntax.DotSpec
    import au.edu.mq.comp.dot.DOTPrettyPrinter.format
    import scala.language.postfixOps
    import au.edu.mq.comp.dot.DOTSyntax._
    import edge.{ DiEdge, LabDiEdge }
    import auto.NFA

    //  convert to DOT using the Rats! parser

    /**
     * Create a DotSpec from an NFA
     *
     * @param   a            The NFA to convert to dot
     *
     * @param   nodeProp     A map from node to a list of attributes.
     *                       The label of a node can be defined by
     *                       `List(Attribute("label"), StringLit(<a string>))`
     *                       where `Attribute` and `StringLit` are defined
     *                       in [[au.edu.mq.comp.dot.DOTSyntax.DotSpec]]
     *
     * @param   nodeDotName  The string to be used for the identifier of a node
     *                       in the dot output. This us useful when nodes have
     *                       weird/long names when converted to string.
     *
     * @param   labelDotName As for `labelDotname` but applied to labels.
     *
     */
    def toDot[ S, L ](
        a : NFA[ S, L ],
        nodeProp : S ⇒ List[ Attribute ] = { s : S ⇒ List[ Attribute ]() },
        nodeDotName : S ⇒ String = { s : S ⇒ s.toString },
        labelDotName : L ⇒ String = { l : L ⇒ l.toString },
        graphProp : () ⇒ List[ Attribute ] = { () ⇒ List() },
        graphDirective : () ⇒ List[ String ] = { () ⇒ List() }
    ) : DotSpec = {
        //  build an DOT automaton
        //  collect edges and make a [[List[EdgeDecl]]]
        lazy val dotEdges = a.transitions map {
            case LabDiEdge( e, l ) ⇒
                SingleTgtEdge(
                    Node( nodeDotName( e.src ) ),
                    Node( nodeDotName( e.tgt ) ),
                    Some( ListAttributes( List( Attribute( "label", StringLit( labelDotName( l ) ) ) ) ) )
                )
        }

        //  collect the nodes in the list of edges
        lazy val nodes = a.edges.foldLeft( Set[ S ]() ) {
            ( s, e ) ⇒ s + ( e.src, e.tgt )
        }

        //  make NodeDecl when list of attributes is not empty
        lazy val dotNodesDecl = nodes collect {
            case n if ( a.accepting.contains( n ) ||
                a.init.contains( n ) ||
                nodeProp( n ).nonEmpty ) ⇒
                ( a.accepting.contains( n ), a.init.contains( n ) ) match {
                    case ( false, false ) ⇒ NodeDecl(
                        Node( nodeDotName( n ) ),
                        ListAttributes( nodeProp( n ) )
                    )

                    case ( true, false ) ⇒ NodeDecl(
                        Node( nodeDotName( n ) ),
                        ListAttributes(
                            Attribute( "prop", Ident( "accepting" ) ) ::
                                Attribute( "shape", Ident( "doublecircle" ) ) :: nodeProp( n )
                        )
                    )

                    case ( false, true ) ⇒ NodeDecl(
                        Node( nodeDotName( n ) ),
                        ListAttributes(
                            Attribute( "prop", Ident( "initial" ) ) :: nodeProp( n )
                        )
                    )

                    case ( true, true ) ⇒ NodeDecl(
                        Node( nodeDotName( n ) ),
                        ListAttributes(
                            Attribute( "prop", Ident( "initial" ) ) ::
                                Attribute( "prop", Ident( "accepting" ) ) :: nodeProp( n )
                        )
                    )
                }
        }

        //  define the graph style
        lazy val dotGraphStyle = {
            val attributes : List[ Attribute ] =
                if ( a.name.nonEmpty )
                    ( Attribute( "label", StringLit( a.name ) ) :: graphProp() )
                else
                    graphProp()
            attributes.map( x ⇒ GraphStyle( ListAttributes( List( x ) ) ) )
        }

        //  graph directives
        lazy val gDirectives = graphDirective().map( x ⇒ GraphDirective( x ) )

        //  create a [[DotSpec]]
        lazy val fullSpec : List[ Block ] =
            gDirectives.toList ++
                dotGraphStyle.toList ++
                dotNodesDecl.toList ++
                dotEdges.toList

        DotSpec( AutomatonName( "automaton" ), AutomatonBody( fullSpec ) )
    }

}
