/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package auto

/**
 * A deterministic and complete automaton (DCA).
 *
 * @param   S   The state type
 * @param   L   The label type
 */
trait DetAuto[ S, L ] {

    /**
     * Name of the automaton
     */
    val name : String

    //  Shortcuts to create compositions of automata.

    /**
     * Disjunction (Union).
     */
    def +[ S1 ]( that : DetAuto[ S1, L ] ) : DetAuto[ ( S, S1 ), L ] =
        Union[ S, S1, L ]( this, that )

    /**
     * Conjunction (synchronised product).
     */
    def *[ S1 ]( that : DetAuto[ S1, L ] ) : DetAuto[ ( S, S1 ), L ] =
        Inter[ S, S1, L ]( this, that )

    /**
     * Difference
     */
    def -[ S1 ]( that : DetAuto[ S1, L ] ) : DetAuto[ ( S, S1 ), L ] =
        Diff[ S, S1, L ]( this, that )

    //  Abstract members for which Concrete methods must be provided

    /**
     * Unique initial state of the automaton.
     */
    def getInit : S

    /**
     * `true` iff `s` is an accepting state of the DCA.
     */
    def isFinal( s : S ) : Boolean

    /**
     * `true` iff every finite word is accepted from `s`.
     *
     * This can be used to defined suffix-closed languages of the
     * form L.A*. IF acceptsAll(`s`) then `isFinal(s)` must also hold.
     */
    def acceptsAll( s : S ) : Boolean

    /**
     * `true` only if no finite word is accepted from `s`.
     *
     *  This is mainly used in building `Difference`: when  the `product`
     *  `A` - `B` is a  state (s1,s2) with acceptsAll(s2) true,
     *  no works can be accepted from (s1,s2). This enables to prune the
     *  search tree when looking for an accepted word in `A`-`B`.
     *
     */
    def acceptsNone( s : S ) : Boolean

    /**
     * Unique successor state `s'` of `s` after `l` in the deterministic view
     */
    def succ( s : S, l : L ) : S

    /**
     * Set of labels in `L` enabled in `s`
     */

    def enabledIn( s : S ) : Set[ L ]

    //   rich interface

    /**
     * Unique successor (deterministic view) after sequence of letters.
     */
    def succ( s : S, xl : Seq[ L ] ) : S = {
        xl.foldLeft( s )( ( x, l ) ⇒ succ( x, l ) )
    }

    import util.Cache
    // cache for outgoing edges
    private val cachedOutGoingEdges = Cache[ S, List[ ( L, S ) ] ]()

    /**
     * Outgoing edges from `s`.
     */
    def outGoingEdges( s : S ) : List[ ( L, S ) ] = {
        cachedOutGoingEdges.putIfNotPresent(
            s,
            enabledIn( s ).map( l ⇒ ( l, succ( s, l ) ) ).toList
        )
        cachedOutGoingEdges.get( s ).get
    }
}
