name := "automat"

version := "0.2-SNAPSHOT"

organization := "au.edu.mq.comp"

// Scala compiler settings

scalaVersion := "2.11.8"

javaOptions in run += "-Dlogback.configurationFile=src/test/resources/logback-test.xml"

scalacOptions :=
    Seq (
        "-deprecation",
        "-feature",
        "-sourcepath", baseDirectory.value.getAbsolutePath,
        "-unchecked",
        "-Xfatal-warnings",
        "-Xlint",
        "-Xcheckinit",
        "-Dfile.encoding=utf-8"
    )

scalacOptions in (Compile, doc) ++= Seq(
    "-groups",
    "-implicits",
    "-diagrams",
    "-diagrams-dot-restart",
    "50")

//testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-oDF")

testOptions in Test <+= (target in Test) map {
  t => Tests.Argument(TestFrameworks.ScalaTest, "-u", "%s" format (t / "../shippable/testresults"))
}

//  scoverage
coverageMinimum := 80
coverageFailOnMinimum := false  //  false is safer as otherwise the build breaks
coverageHighlighting := true    //  enable highlighting of covered/non-covered
//  exclude some package from coverage
coverageExcludedPackages := ".*sbtrats.*"

// Interactive settings

parallelExecution in Test := true

logLevel := Level.Info

shellPrompt <<= (name, version) { (n, v) =>
     _ => n + " " + v + "> "
}

// Dependencies

libraryDependencies ++=
    Seq (
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.0.0",
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.0.0" % "test" classifier ("tests"),
        "org.scalatest" %% "scalatest" % "2.2.4" % "test",
        "org.scalacheck" %% "scalacheck" % "1.12.2" % "test",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
       "ch.qos.logback" % "logback-classic" % "1.1.6"
    )


resolvers ++= Seq (
    Resolver.sonatypeRepo ("releases"),
    Resolver.sonatypeRepo ("snapshots")
)

// Rats! setup

sbtRatsSettings

ratsScalaRepetitionType := Some (ListType)

ratsUseScalaOptions := true

ratsUseScalaPositions := true

ratsDefineASTClasses := true

ratsDefinePrettyPrinter := true

ratsUseKiama := 2

// ScalariForm

import scalariform.formatter.preferences._
import SbtScalariform.ScalariformKeys

scalariformSettings
//scalariformSettingsWithIt

ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference (AlignSingleLineCaseStatements, true)
    .setPreference (IndentSpaces, 4)
    .setPreference (SpaceBeforeColon, true)
    .setPreference (SpaceInsideBrackets, true)
    .setPreference (SpacesAroundMultiImports, true)
    .setPreference (PreserveSpaceBeforeArguments, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference (RewriteArrowSymbols, true)
    .setPreference (SpaceInsideParentheses, true)
//    .setPreference (PreserveSpaceAfterArguments, true)
