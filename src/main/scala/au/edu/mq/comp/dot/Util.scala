/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.dot
package util

/**
 * Provide methods to collect information from a [[DOTSyntax.DotSpec]]
 * and build an [[au.edu.mq.comp.automat.auto.NFA]]
 *
 * Information that can be retrieved is:
 * - name of the automaton (Dot digraph)
 * - set of edges
 * - initial states
 * - final states
 * Initial and final states  should be defined as node properties in
 * the form node [prop=final,some other attributes] ; list of nodes
 */
object DotASTAttr {

    import DOTSyntax._
    import au.edu.mq.comp.automat.edge.{ DiEdge, LabDiEdge }

    /**
     * Name of the main automaton (digraph) in a DotSpec
     *
     * @param   A A [[DotSpec]]
     * @return    The name of the digraph
     */
    val dotName : DotSpec ⇒ String = { case DotSpec( AutomatonName( x ), _ ) ⇒ x }

    /**
     * Get initial states
     *
     * @param s a [[DotSpec]]
     * @return the set of initial states, nodeName [prop=initial]
     */
    val getInitStates : DotSpec ⇒ Set[ String ] = {
        case s ⇒ getNodes( s )( Attribute( "prop", Ident( "initial" ) ) )
    }

    /**
     * Get accepting states
     *
     * @param s - a [[DotSpec]]
     * @return The set of accepting states nodeNale [prop=accepting]
     */
    val getAcceptStates : DotSpec ⇒ Set[ String ] = {
        case s ⇒ getNodes( s )( Attribute( "prop", Ident( "accepting" ) ) )
    }

    //  Retrieve label (if any) in a list of attributes, component of the
    //  form label="l" in the list
    private val getLabel : List[ Attribute ] ⇒ Option[ String ] = {
        case xa ⇒ ( xa find {
            case Attribute( x, y ) ⇒ ( x == "label" )
        } ) map { case Attribute( x, StringLit( y ) ) ⇒ y }
    }

    import au.edu.mq.comp.automat.edge.Implicits._
    /**
     * Compute the list of edges of a DotSpec automaton
     *
     * @return List of edges of type
     */
    val getEdges : DotSpec ⇒ Set[ LabDiEdge[ String, String ] ] = {
        case DotSpec( _, AutomatonBody( xl ) ) ⇒ ( xl collect {
            case MultipleTgtEdge( Node( x ), Nodes( xn ), xa ) ⇒
                val l : Option[ String ] = xa match {
                    case None                         ⇒ None
                    case Some( ListAttributes( la ) ) ⇒ getLabel( la )
                }
                ( xn map { case Node( y ) ⇒ ( x ~> y )( l.getOrElse( "" ) ) } ).toSet
            case SingleTgtEdge( Node( x ), Node( y ), xa ) ⇒
                val l : Option[ String ] = xa match {
                    case None                         ⇒ None
                    case Some( ListAttributes( la ) ) ⇒ getLabel( la )
                }
                Set( ( x ~> y )( l.getOrElse( "" ) ) )
        } ) flatMap ( identity ) toSet
    }

    /**
     * Compute the list of nodes declaration of a DotSpec automaton that
     * have an attribute value
     *
     * Usage: collect initial and accepting nodes
     *
     * @return List of NodeDecl
     */
    private val getNodes : DotSpec ⇒ Attribute ⇒ Set[ String ] = {
        case DotSpec( _, AutomatonBody( xl ) ) ⇒ {
            case a ⇒ xl collect {
                case NodeDecl( Node( x ), ListAttributes( xa ) ) if xa.contains( a ) ⇒ x
            } toSet
        }
    }
}
