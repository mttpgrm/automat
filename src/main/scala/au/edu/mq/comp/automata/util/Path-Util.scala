package au.edu.mq.comp.automat

package util

/**
 * Utilities for managing paths that are used to load and store dot files for testing.
 */
object PathUtil {
    import java.nio.file.{ Files, Paths }

    /** Path to dot tests files directory which is used by dot parser tests */
    val dotTestPath = Paths.get( "src", "test", "scala", "au", "edu", "mq", "comp", "automata", "dot-test-files" ).toString()

    /** Join a filename to a path stored in a string */
    def joinPath( path : String, fileName : String ) = Paths.get( path, fileName ).toString()

    /**
     *  Returns a path to the specified file name within the directory "tmp". "tmp" directory is
     *  created if it does not exist
     */
    def tmpPath( fileName : String ) = {

        if ( !Files.exists( Paths.get( "tmp" ) ) )
            Files.createDirectory( Paths.get( "tmp" ) )

        Paths.get( "tmp", fileName ).toString()
    }
}

