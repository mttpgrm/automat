/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package util

/**
 * Provide a function to compute an NFA[Int,L]
 * representation of an DetAuto[S,L]
 */
object Determiniser {

    import Traversal._
    import edge.{ LabDiEdge }
    import edge.Implicits._
    import auto.{ NFA, DetAuto }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    /**
     * Determinise an automaton.
     *
     * @param a The automaton DetAuto[S,L] to be determinised
     * @return Return an automaton of type NFA[Int,L] which is deterministic
     */
    def toDetNFA[ S, L ]( a : DetAuto[ S, L ] ) : NFA[ Int, L ] = {

        logger.info( "Starting Determinisation of automaton {}", a.name )

        //  compute the determinised version with a visitor and collect states and edges
        val DFSdeterminiser = DFS( DeterminiserVisitor[ S, L ](), a.outGoingEdges )
        val v = DFSdeterminiser( a.getInit )

        logger.info( "Determinisation completed for automaton {}", a.name )

        //  create an NFA
        new NFA(
            Set( v.index( a.getInit ) ),
            v.edges.toSet,
            ( for ( ( s, i ) ← v.index if a.isFinal( s ) ) yield i ).toSet,
            ( for ( ( s, i ) ← v.index if a.acceptsAll( s ) ) yield i ).toSet,
            ( for ( ( s, i ) ← v.index if a.acceptsNone( s ) ) yield i ).toSet
        )
    }

    /**
     * Visitor to collect the reachable states and edges of a [[DetAuto[S,L]]] and build a [[NFA[Int,L]]]
     *
     * @param index Each new state discovered during the DFS is assigned a fresh integer index
     * @return edges The edges of the dterminisation of the automaton
     */
    private case class DeterminiserVisitor[ S, L ](
            val index : Map[ S, Int ] = Map[ S, Int ](),
            val edges : List[ LabDiEdge[ Int, L ] ] = List[ LabDiEdge[ Int, L ] ]()
    ) extends DFSVisitor[ S, L, DeterminiserVisitor[ S, L ] ] {

        //  When discovering a new state s, if it does not have a parent
        //  we do not create a new edge as it should be the initial state.
        //  otherwise, if p(s) = (t, l) add edge (s, t, l) but mapped to indices.
        override def discoverState(
            s : S,
            discoveryTime : Map[ S, Int ],
            parent : Map[ S, ( S, L ) ]
        ) = {
            val newIndex = index + ( s → discoveryTime( s ) )
            val newEdges =
                if ( parent.isDefinedAt( s ) )
                    (
                        ( newIndex( parent( s )._1 ) ~> newIndex( s ) )( parent( s )._2 )
                    ) :: edges
                else edges
            DeterminiserVisitor( newIndex, newEdges )
        }

        //  Nothing to be done on treeEdges as the new edge will be created only when we visit
        //  the new state
        override def treeEdge(
            s : S, l : L, t : S,
            discoveryTime : Map[ S, Int ],
            parent : Map[ S, ( S, L ) ]
        ) = this

        //  For backEdges and ForwardEdges, no new node is created, only  add an edge.
        override def backEdge(
            s : S, l : L, t : S,
            discoveryTime : Map[ S, Int ],
            parent : Map[ S, ( S, L ) ]
        ) = DeterminiserVisitor( index, ( index( s ) ~> index( t ) )( l ) :: edges )

        //  See comment for `backEdge`
        override def forwardEdge(
            s : S, l : L, t : S,
            discoveryTime : Map[ S, Int ],
            parent : Map[ S, ( S, L ) ]
        ) = DeterminiserVisitor( index, ( index( s ) ~> index( t ) )( l ) :: edges )

    }
}
