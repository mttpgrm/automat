package au.edu.mq.comp.automat

package util

import au.edu.mq.comp.dot.DOTSyntax.DotSpec
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import org.scalatest._
import au.edu.mq.comp.dot.DOTPrettyPrinter.format
import DotConverter._
import reflect.io._
import au.edu.mq.comp.dot.util.FileParser.parseFile
import scala.util.{ Try, Success, Failure }
import au.edu.mq.comp.dot.util.DotASTAttr._
import au.edu.mq.comp.dot.DOTSyntax.{ Attribute, StringLit, Ident }

import util.PathUtil.tmpPath

/**
 *  test for conversion from NFA toDot format
 */
class NFAToDotTests extends FunSuite with Matchers {

    test( "Convert NFA to DOT, read the file and compare to initial" ) {
        //   as we generate a dot file and the type of node
        //   read from DOT is String, we create an automaton with
        //   nodes of type strings
        val nfa1 = NFA(
            init = Set( "0" ),
            Set(
                ( "0" ~> "1" )( "a" ), ( "0" ~> "2" )( "a" ), ( "1" ~> "0" )( "b" ), ( "1" ~> "2" )( "a" )
            ),
            Set( "2" )
        )

        val nfa1Dot : DotSpec = toDot( nfa1 )
        //  print automaton in a file
        File( tmpPath ( "nfa1.dot" ) ).writeAll( format( nfa1Dot ).layout )

        // now parse file and check it is the same as the automaton we started
        // with
        val tryNfa : Try[ DotSpec ] = parseFile( tmpPath ( "nfa1.dot" ) )

        assert( tryNfa.isSuccess )
        //  build an NFA. As we read strings we have to convert
        val readInnfa1 = NFA(
            getInitStates( tryNfa.get ),
            getEdges( tryNfa.get ), getAcceptStates( tryNfa.get )
        )
        assert( nfa1 == readInnfa1 )
    }

    //  @tony: example with node attributes
    test( "Output an NFA with node attributes" ) {
        val nfa2 = NFA(
            init = Set( "0" ),
            Set(
                ( "0" ~> "1" )( "a" ), ( "0" ~> "2" )( "a" ), ( "1" ~> "0" )( "b" ), ( "1" ~> "2" )( "a" )
            ),
            Set( "2" ), name = "NFA2"
        )

        val nfa2Dot : DotSpec =
            toDot(
                nfa2,
                //  map for node labels
                //  this is the label that will be displayed in Dot
                nodeProp = {
                x : String ⇒
                    if ( !nfa2.accepting.contains( x ) )
                        List( Attribute( "label", StringLit( "node" + x ) ) )
                    else
                        List(
                            Attribute( "shape", Ident( "doublecircle" ) ),
                            Attribute( "label", StringLit( "node" + x ) )
                        )
            },
                //  map for node identifiers
                //  this is the node ID e.g. edges will be output
                //  as nodeIDsrc -> nodeIDtgt [some edge attributes]
                labelDotName = {
                x : String ⇒ "Node_" + x
            },
                graphDirective = {
                () ⇒ List( "rank = sink ; 0 ", "rank = source ; 2" )
            }
            )

        File( tmpPath ( "nfa2.dot" ) ).writeAll( format( nfa2Dot ).layout )

    }

}
