package au.edu.mq.comp.automat

package lang

import util.Traversal.DFS
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import util.Determiniser.toDetNFA
import scala.language.existentials

import org.scalatest._

class SimpleLangTests extends FunSuite with Matchers {

    test( "Empty automaton and language" ) {
        //  one automaton, empty
        val nfa1 = NFA[ Int, String ]( Set(), Set(), Set() )
        val l1 = Lang( nfa1 )

        val e = l1.getAcceptedTrace

        assert( l1.isEmpty )

    }

    test( "Automaton accepting empty word" ) {
        //  one automaton, accepting empty word
        val nfa2 = NFA[ Int, String ]( Set( 0 ), Set(), Set( 0 ) )
        val l2 = Lang( nfa2 )

        val e = l2.getAcceptedTrace

        assert( !e.isEmpty )
        assert( e.get == List() )

        assert( l2 accepts e.get )
    }

    test( "Automaton accepting a non empty word" ) {
        val nfa3 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 3 )( "a" ), ( 1 ~> 0 )( "a" ), ( 1 ~> 2 )( "b" )
            ),
            Set( 2 )
        )
        //  one automaton, accepting empty word
        val l3 = Lang( nfa3 )

        val e = l3.getAcceptedTrace

        assert( !e.isEmpty )

        assert( e.get == List( "a", "b" ) )

        assert( l3 accepts e.get )
        assert( l3 accepts List( "a", "a", "a", "b" ) )
        assert( l3 accepts List( "a", "a", "a", "a", "a", "b" ) )
    }

    test( "Automaton accepting non empty traces and suffix acceptance" ) {
        val nfa4 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 3 )( "a" ), ( 1 ~> 0 )( "a" ), ( 1 ~> 2 )( "b" )
            ),
            Set( 2 )
        )
        //  one automaton, accepting empty word
        val l4 = Lang( nfa4 )

        //  find an accepting trace following a,b
        val e = l4.getAcceptedTraceAfter( List( "a", "b" ) )
        //  should be non empty as epsilon (empty word) is accepted
        assert( !e.isEmpty )
        //  check that we got epsilon
        assert( e.get == List() )

        //  find an accepting trace following a,b
        val e1 = l4.getAcceptedTraceAfter( List() )
        //  should be non empty as epsilon (empty word) is accepted
        assert( !e1.isEmpty )
        //  check that we got epsilon
        assert( e1.get == List( "a", "b" ) )

        //  compute quotient language
        val l5 = l4 / List( "a" )
        assert( l5 accepts List( "b" ) )
    }

    test( "Language equality test" ) {
        val nfa4 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 3 )( "a" ), ( 1 ~> 0 )( "a" ), ( 1 ~> 2 )( "b" )
            ),
            Set( 2 )
        )
        val det = toDetNFA( nfa4 )

        assert( Lang( nfa4 ) === Lang( det ) )
    }
}
