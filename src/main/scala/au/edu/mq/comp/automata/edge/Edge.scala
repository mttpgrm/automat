/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package edge

/**
 * An edge in a directed graph which is a pair `(source, target)`.
 *
 * @param   src  The source node
 * @param   tgt  The target node
 * @tparam  S   Type of the nodes
 */
case class DiEdge[ S ]( val src : S, val tgt : S ) {
    /**
     * Reverse the direction of an edge
     */
    def reversed = new DiEdge[ S ]( tgt, src )

}

/**
 * A labelled  edge in a directed graph is a triple `(source, label, target)`.
 *
 * @param   src     The source node
 * @param   tgt     The target node
 * @param   lab     The label
 * @tparam  S       Type of the nodes
 * @tparam  L       Type of the labels
 * @note            use L = Option[Nothing] and None for unlabelled graphs
 */
case class LabDiEdge[ S, L ]( unLabelled : DiEdge[ S ], lab : L ) {

    /**
     * secondary constructor
     */
    def this( src : S, l : L, tgt : S ) = this ( new DiEdge( src, tgt ), l )

    def src = unLabelled.src

    def tgt = unLabelled.tgt

    /**
     * Reverse a directed edge.
     */
    def reversed : LabDiEdge[ S, L ] = LabDiEdge( unLabelled.reversed, lab )
}

/**
 *  Create edges using a friendly syntax.
 */
object Implicits {

    /**
     * Create edges using x ~> y.
     */
    implicit class easyEdge[ S ]( s : S ) {
        def ~>( t : S ) : DiEdge[ S ] = new DiEdge( s, t )
    }

    /**
     * Create labelled edges using (x ~> y)(l).
     */

    implicit class LabDiEdgeConstructor[ S, L ]( e : DiEdge[ S ] ) {
        def apply( label : L ) = LabDiEdge[ S, L ]( e, label )
    }
}
