package au.edu.mq.comp.automat

package util

import org.scalatest._
import edge.LabDiEdge
import edge.Implicits._
import auto.NFA
import Traversal.DFS
import graph.Dominator.{ tarjanImDom, domFrontier }
import graph.RootedDiGraph
import au.edu.mq.comp.dot.DOTPrettyPrinter.format
import DotConverter._
import au.edu.mq.comp.dot.DOTSyntax.DotSpec
import reflect.io._

import au.edu.mq.comp.dot.util.DotASTAttr._
import au.edu.mq.comp.dot.DOTSyntax.{ Attribute, StringLit, Ident }

import util.PathUtil.tmpPath

class DominatorTests extends FunSuite with Matchers {

    def prettyPrint[ S ]( root : S, s : Map[ S, S ] ) : DotSpec = {
        val a = new NFA[ S, Option[ Nothing ] ](
            Set( root ),
            ( s map {
                case ( src, tgt ) ⇒ ( src ~> tgt )( None : Option[ Nothing ] )
            } ).toSet,
            Set()
        )

        val aDot : DotSpec =
            toDot(
                a,
                //  map for node labels
                //  this is the label that will be displayed in Dot
                nodeProp = {
                x : S ⇒
                    List(
                        // Attribute("shape", Ident("doublecircle")),
                        Attribute( "label", StringLit( x.toString ) )
                    )
            },
                //  map for node identifiers
                //  this is the node ID e.g. edges will be output
                //  as nodeIDsrc -> nodeIDtgt [some edge attributes]
                nodeDotName = {
                x : S ⇒ "\"" + x.toString + "\""
            },
                labelDotName = {
                x : Option[ Nothing ] ⇒ ""
            },
                graphProp = {
                () ⇒ List( Attribute( "rankdir", Ident( "TB" ) ) )
            },
                graphDirective = {
                () ⇒ List( "rank = sink ; \"" + root.toString + "\"" )
            }
            )
        aDot
    }

    test( "NFA with no loop, compute post dominator tree" ) {

        val nfa1 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "a" ), ( 1 ~> 2 )( "b" ), ( 2 ~> 3 )( "c" )
            ),
            Set( 2 )
        )

        val nfa1Dot : DotSpec = toDot( nfa1 )
        //  dump automaton in a file
        File( tmpPath ( "nfaDom1.dot" ) ).writeAll( format( nfa1Dot ).layout )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 3, nfa1.reversed ) )
        val domDot : DotSpec = prettyPrint( 3, postDomTree )
        File( tmpPath ( "dominator1.dot" ) ).writeAll( format( domDot ).layout )

        val postDomFront = domFrontier( RootedDiGraph( 3, nfa1 ).reversed )
        assert( postDomFront === Map( 2 → Set(), 1 → Set( 0 ), 0 → Set(), 3 → Set() ) )
    }

    test( "NFA with no loops, if-else-like, compute post dominator tree" ) {
        val nfa2 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 0 ~> 2 )( "b" ), ( 2 ~> 3 )( "b" ), ( 1 ~> 3 )( "a" )
            ),
            Set( 3 )
        )

        val nfa2Dot : DotSpec = toDot( nfa2 )
        //  dump automaton in a file
        File( tmpPath ( "nfaDom2.dot" ) ).writeAll( format( nfa2Dot ).layout )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 3, nfa2 ).reversed )
        val domDot : DotSpec = prettyPrint( 3, postDomTree )
        File( tmpPath( "dominator2.dot" ) ).writeAll( format( domDot ).layout )

        val postDomFront = domFrontier( RootedDiGraph( 3, nfa2 ).reversed )
        assert( postDomFront === Map( 2 → Set( 0 ), 1 → Set( 0 ), 3 → Set(), 0 → Set() ) )
    }

    test( "Example from Tarjan's paper" ) {
        val nfa3 = NFA[ String, String ](
            Set(),
            Set(
                ( "I" ~> "K" )( "" ),
                ( "K" ~> "I" )( "" ),
                ( "J" ~> "G" )( "" ),
                ( "I" ~> "F" )( "" ),
                ( "I" ~> "G" )( "" ),
                ( "I" ~> "J" )( "" ),
                ( "F" ~> "C" )( "" ),
                ( "G" ~> "C" )( "" ),
                ( "R" ~> "K" )( "" ),
                ( "K" ~> "H" )( "" ),
                ( "E" ~> "H" )( "" ),
                ( "H" ~> "E" )( "" ),
                ( "H" ~> "L" )( "" ),
                ( "L" ~> "D" )( "" ),
                ( "A" ~> "B" )( "" ),
                ( "A" ~> "R" )( "" ),
                ( "D" ~> "A" )( "" ),
                ( "D" ~> "B" )( "" ),
                ( "E" ~> "B" )( "" ),
                ( "C" ~> "R" )( "" ),
                ( "B" ~> "R" )( "" )
            ),
            Set( "R" )
        )

        val nfa3Dot : DotSpec = toDot( nfa3 )
        //  dump automaton in a file
        File( tmpPath ( "nfaDom3.dot" ) ).writeAll( format( nfa3Dot ).layout )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( "R", nfa3 ).reversed )
        val domDot : DotSpec = prettyPrint( "R", postDomTree )
        File( tmpPath ( "dominator3.dot" ) ).writeAll( format( domDot ).layout )

        val postDomFront = domFrontier( RootedDiGraph( "R", nfa3 ).reversed )
        assert(
            postDomFront ===
                Map( "E" → Set( "H" ), "J" → Set( "I" ),
                    "F" → Set( "I" ), "A" → Set( "D" ),
                    "I" → Set( "K" ), "G" → Set( "I" ),
                    "L" → Set( "H" ), "B" → Set( "E", "A", "D" ),
                    "C" → Set( "I" ), "H" → Set( "E", "K" ),
                    "K" → Set( "I" ), "R" → Set(), "D" → Set( "H" ) )
        )
    }

    test( "Linear NFA with no loop, compute post dominator tree" ) {
        val nfa4 = NFA[ Int, String ](
            Set( 0 ),
            Set(
                ( 0 ~> 1 )( "a" ), ( 1 ~> 2 )( "a" ), ( 2 ~> 3 )( "b" ), ( 3 ~> 4 )( "a" )
            ),
            Set( 4 )
        )

        val nfa4Dot : DotSpec = toDot( nfa4 )
        //  dump automaton in a file
        File( tmpPath ( "nfaDom4.dot" ) ).writeAll( format( nfa4Dot ).layout )

        //  for post dominators we use the dominator algorithm from a
        //  in the reverse graph
        val postDomTree = tarjanImDom( RootedDiGraph( 4, nfa4 ).reversed )
        val domDot : DotSpec = prettyPrint( 4, postDomTree )
        File( tmpPath ( "dominator4.dot" ) ).writeAll( format( domDot ).layout )

        val postDomFront = domFrontier( RootedDiGraph( 4, nfa4 ).reversed )
        assert( postDomFront === Map( 0 → Set(), 1 → Set(), 2 → Set(), 3 → Set(), 4 → Set() ) )

    }
}
