/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package au.edu.mq.comp.automat
package util

/**
 * Provide DFS traversal with early termination and visitors to collect data.
 */
object Traversal {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    /**
     * Performs Depth-First Search
     * @param   v    A visitor
     * @param   next A function that defines the transition relation
     * @return       A function from `S` to `V`. When called performs a
     *               DFS from `s` and returns a `V`
     */
    def DFS[ S, L, V <: DFSVisitor[ S, L, V ] ]( v : V, next : S ⇒ List[ ( L, S ) ] ) : S ⇒ V = {

        case s ⇒
            //  local definition of recursive DFS

            /*
             * Performs tail recursive Depth-First Search
             *
             * @param   stack         Current state of the search
             *
             * @param   colourMap     Gives the colour of each state on the stack
             *                        If undefined then it is White (not seen yet),
             *                        false then Gray  (on the stack and not fully
             *                        explored) and true then Black (all successors
             *                        explored and thus fully explored)
             *
             * @param   time          Current discovery time (updated every time a
             *                        new state is  encountered).
             *
             * @param   discoveryTime The map that gives the discorery time for each state.
             *
             * @param   parent        The parent Map for the currently explored
             *                        sub spanning tree.
             *
             * @param   a A visitor [[DFSVisitor]]
             */
            import scala.annotation.tailrec
            @tailrec def DFSRec(
                stack : List[ ( S, List[ ( L, S ) ] ) ], //  current state of DFS
                colourMap : Map[ S, Boolean ], //  map discovered states to Colour
                time : Int, //  current logical time in the DFS
                discoveryTime : Map[ S, Int ], //  discovery time of a discovered state
                parent : Map[ S, ( S, L ) ], //  parent in the DFS sub spanning tree
                v : V
            ) //  visitor to collect info
            : V = {

                //  Colours to perform DFS
                sealed trait Colours
                object White extends Colours
                object Black extends Colours
                object Gray extends Colours
                //  helper to manage states' colours
                @inline def colour( s : S ) = colourMap.get( s ) match {
                    case Some( false ) ⇒ Gray
                    case Some( true )  ⇒ Black
                    case None          ⇒ White
                }

                //  If visitor says search is over then stop and return current result
                if ( v.abortSearch ) {
                    logger.info( "Aborting DFS" )
                    v
                } //  otherwise continue search
                else
                    //  DFS algorithm re-starts now
                    stack match {

                        // empty stack. DFS is completed and we return the Visitor v
                        case Nil ⇒
                            logger.info( "Empty stack. DFS completed" )
                            v

                        //  stack is not empty, process state x at top of the stack
                        case ( x, xe ) :: xs ⇒ {
                            colour( x ) match {

                                //  x is a new state not discovered yet
                                case White ⇒
                                    logger.info( s"Discovered White state $x" )
                                    DFSRec(
                                        ( x, next( x ) ) :: xs, colourMap + ( x → false ), time + 1,
                                        discoveryTime + ( x → time ), parent,
                                        v.discoverState( x, discoveryTime + ( x → time ), parent )
                                    )

                                //  we have already discovered x. Check transitions xe that remain to be explored
                                case Gray ⇒ xe match {
                                    //  no more transitions. x is fully explored
                                    case Nil ⇒
                                        logger.info( s"State $x fully explored" )
                                        DFSRec( xs, colourMap + ( x → true ), time,
                                            discoveryTime, parent, v.finishState( x, discoveryTime, parent ) )

                                    //  some transitions left
                                    case ( l, tgt ) :: c ⇒ colour( tgt ) match {
                                        //  tgt is a new state
                                        case White ⇒
                                            logger.info( s"New target state $tgt" )
                                            DFSRec( ( tgt, List() ) :: ( x, c ) :: xs, colourMap, time,
                                                discoveryTime, parent + ( tgt → ( ( x, l ) ) ),
                                                v.treeEdge( x, l, tgt, discoveryTime, parent ) )

                                        //  tgt has already been discovered and is on the stack (not fully explored)
                                        case Gray ⇒
                                            logger.info( s"Already discovered target state $tgt" )
                                            DFSRec( ( x, c ) :: xs, colourMap, time,
                                                discoveryTime, parent, v.backEdge( x, l, tgt, discoveryTime, parent ) )

                                        //  tgt has already been fully explored
                                        case Black ⇒
                                            logger.info( s"Already fully explored target state $tgt" )
                                            DFSRec( ( x, c ) :: xs, colourMap, time,
                                                discoveryTime, parent, v.forwardEdge( x, l, tgt, discoveryTime, parent ) )
                                    }
                                }

                                //  case Black should not happen as every state pushed on the stack is not Black
                                case Black ⇒
                                    logger.error( s"Black node $x on the Stack in DFS." )
                                    throw new RuntimeException( "Black node on the Stack in DFS." )
                            }
                        }
                    }
            }

            //  start the DFS
            DFSRec( List( ( s, List() ) ), Map(), 0, Map(), Map(), v )
    }
}
